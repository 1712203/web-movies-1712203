var cur_page = 1;
var type_search = 1;

// Call default page
function callDefaultPage(page)
{
    $('.container').empty();
    $('.container').append(`
        <div class="loader" id="loader-2">
          <span></span>
          <span></span>
          <span></span>
        </div>
    `)
    var temp1;
    temp1 = setTimeout(defaultPage, 1000, page);
}



// Pagination for default page
async function pagintionDefaultPage(pages)
{
    $('.pagination').empty();

    $('.pagination').append(`<a href="#" style="width: 50px; text-align:center">&laquo;</a>`);

    if (type_search == 1) {
        var i = 1;
        for (i; i <= pages; i++) {
            $('.pagination').append(`
            <a href="#" id="page_link${i}" style="width: 50px; text-align:center" onclick="callDefaultPage(${i})">${i}</a>`)
        }
    }
    else {
        var i = 1;
        for (i; i <= pages; i++) {
            $('.pagination').append(`
            <a href="#" id="page_link${i}" style="width: 50px; text-align:center" onclick="callDefaultPage(${i})">${i}</a>`)
        }
    }

    $('.pagination').append(`<a href="#" style="width: 50px; text-align:center">&raquo;</a>`)
}


// default Page
async function defaultPage(page)
{
    $('.container').empty();

    const response = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=c07b3118bd893458a9c721e838c62af0&page=${page}`);
    const data = await response.json();
    const list = data.results;
    const pages = data.total_pages;

    for (const element of list) {
        var temp1 = "https://image.tmdb.org/t/p/w500" + element.poster_path;
        var temp2 = element.release_date;
        if (element.poster_path == null) {
            temp1 = "images/movie.jpg";
        }
        if (element.release_date == null) {
            temp2 = "01-01-2000";
        }      

        $('.container').append(`
        <div class="col-sm-3" style="margin-bottom: 10px;">
            <div class="col-sm-12" style="border: 3px solid red; background: white">
                <div class="card">
                     <img class="card-img-top" alt="Poster of ${element.title}" src="${temp1}" width="215" height="300" style="margin-top:10px" onclick="detailMovie('${element.id}')">
                     <div class="card-body" style="height: 150px">
                         <h4 class="card-title" style="font-weight: bold; color: black;" align="center" >${element.original_title}</h4>
                         <p class="card-text" align="center" style="color: black;">${element.popularity}&#128065;</p>   
                         <p class="card-text" align="center" style="color: black;">${temp2}</p>                    
                         <p class="card-text" align="center" style="color: blue;">Rate: ${element.vote_average}&#9733</p>
                    </div>
                 </div>
            </div>
        </div>
        `)
    }

    // var temp2;
    // temp2 = setTimeout(pagintionDefaultPage, 0, pages);
    pagintionDefaultPage(pages);

    document.getElementById(`page_link${cur_page}`).style.backgroundColor = "black";
    document.getElementById(`page_link${page}`).style.backgroundColor = "red";
    cur_page = page;
}

//-----------------------Step 2-----------------------------------------------------------------------
// Search movie by people or movie name
async function Search(event) {
    event.preventDefault();
    var name = document.getElementById("searchVal").value;

    $('.container').empty();
    $('.pagination').empty();

    if (type_search == 1) {
        const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=c07b3118bd893458a9c721e838c62af0&query=${name}`);
        const data = await response.json();
        const pages = data.total_pages;
        var temp2;
        cur_page = 1;

        callAddFilmFunc(1, name);
        temp2 = setTimeout(addPagination, 1000, pages, name);
    }
    else {
        const response = await fetch(`https://api.themoviedb.org/3/search/person?api_key=c07b3118bd893458a9c721e838c62af0&query=${name}`);
        const data = await response.json();
        const pages = data.total_pages;
        var temp2;
        cur_page = 1;

        callAddFilmFuncByActor(1, name);
        temp2 = setTimeout(addPagination, 1000, pages, name);
    }

}

// Call function add film by name
function callAddFilmFunc(page, name) {
    $('.container').empty();
    $('.container').append(`
        <div class="loader" id="loader-2">
          <span></span>
          <span></span>
          <span></span>
        </div>
    `)
    var temp1;
    temp1 = setTimeout(addFilm, 1000, page, name);
}

// Add Film By Name
async function addFilm(page, name) {

    $('.container').empty();

    const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=c07b3118bd893458a9c721e838c62af0&query=${name}&page=${page}`);
    const data = await response.json();
    const list = data.results;


    for (const element of list) {
        var temp1 = "https://image.tmdb.org/t/p/w500" + element.poster_path;
        var temp2 = element.release_date;
        if (element.poster_path == null) {
            temp1 = "images/movie.jpg";
        }
        if (element.release_date == null) {
            temp2 = "01-01-2000";
        }
        $('.container').append(`
        <div class="col-sm-3" style="margin-bottom: 10px;">
            <div class="col-sm-12" style="border: 3px solid red; background: white">
                <div class="card">
                     <img class="card-img-top" alt="Poster of ${element.title}" src="${temp1}" width="215" height="300" style="margin-top:10px" onclick="detailMovie('${element.id}')">
                      <div class="card-body" style="height: 150px">
                         <h4 class="card-title" style="font-weight: bold; color: black;" align="center" >${element.original_title}</h4>
                         <p class="card-text" align="center" style="color: black;">${element.popularity}&#128065;</p>
                         <p class="card-text" align="center" style="color: black;">${temp2}</p>
                         <p class="card-text" align="center" style="color: blue;">Rate: ${element.vote_average}&#9733</p>
                    </div>
                 </div>
            </div>
        </div>
        `)
    }

    document.getElementById(`page_link${cur_page}`).style.backgroundColor = "black";
    document.getElementById(`page_link${page}`).style.backgroundColor = "red";
    cur_page = page;
}

// Call function add film by people
function callAddFilmFuncByActor(page, name) {
    $('.container').empty();
    $('.container').append(`
        <div class="loader" id="loader-2">
          <span></span>
          <span></span>
          <span></span>
        </div>
    `)
    var temp1;
    temp1 = setTimeout(addFilmByActor, 1000, page, name);
}

// Add Film By Actor's name
async function addFilmByActor(page, name) {
    $('.container').empty();
    const response = await fetch(`https://api.themoviedb.org/3/search/person?api_key=c07b3118bd893458a9c721e838c62af0&query=${name}&page=${page}`);
    const data = await response.json();
    const t = data.results;

    for (const result of t) {
        const list = result.known_for;
        for (const element of list) {
            var temp1 = "https://image.tmdb.org/t/p/w500" + element.poster_path;
            var temp2 = element.release_date;
            if (element.poster_path == null) {
                temp1 = "images/movie.jpg";
            }
            if (element.release_date == null) {
                temp2 = "01-01-2000";
            }
            $('.container').append(`
                <div class="col-sm-3" style="margin-bottom: 10px;">
                    <div class="col-sm-12" style="border: 3px solid red; background: white">
                        <div class="card">
                            <img class="card-img-top" alt="Poster of ${element.title}" src="${temp1}" width="215" Height="300" style="margin-top:10px" onclick="detailMovie('${element.id}')">
                            <div class="card-body" style="height: 150px">
                                <h4 class="card-title" style="font-weight: bold; color: black;" align="center" >${element.original_title}</h4>
                                <p class="card-text" align="center" style="color: black;">${element.popularity}&#128065;</p>
                                <p class="card-text" align="center" style="color: black;">${temp2}</p>
                                <p class="card-text" align="center" style="color: blue;">Rate: ${element.vote_average}&#9733</p>
                            </div>
                        </div>
                    </div>
                </div>
        `)
        }
    }

    document.getElementById(`page_link${cur_page}`).style.backgroundColor = "black";
    document.getElementById(`page_link${page}`).style.backgroundColor = "red";
    cur_page = page;
}


// Pagination for film
async function addPagination(pages, name) {

    $('.pagination').empty();

    $('.pagination').append(`<a href="#" style="width: 50px; text-align:center" onclick="NextPage(${cur_page},${pages},'${name}')">&laquo;</a>`);

    if (type_search == 1) {
        var i = 1;
        for (i; i <= pages; i++) {
            $('.pagination').append(`
            <a href="#" id="page_link${i}" style="width: 50px; text-align:center" onclick="callAddFilmFunc(${i},'${name}')">${i}</a>`)
        }
    }
    else {
        var i = 1;
        for (i; i <= pages; i++) {
            $('.pagination').append(`
            <a href="#" id="page_link${i}" style="width: 50px; text-align:center" onclick="callAddFilmFuncByActor(${i},'${name}')">${i}</a>`)
        }
    }

    $('.pagination').append(`<a href="#" style="width: 50px; text-align:center" onclick="PreviousPage(${cur_page},${pages},'${name})'">&raquo;</a>`)
}

// Next Page film
function NextPage(cur_page, pages, name) {
    if (cur_page < pages) {
        callAddFilmFunc(cur_page + 1, name);
        cur_page = cur_page + 1;
    }
}

// Previous Page film
function PreviousPage(cur_page, pages, name) {
    if (cur_page > 1) {
        callAddFilmFunc(cur_page - 1, name);
        cur_page = cur_page - 1;
    }
}

function ChangeStyleSearch(type) {
    // Film
    if (type == 1) {
        type_search = 1;
        document.getElementById('type-film').className = "active"
        document.getElementById('type-actor').className = "not-active";
    }
    // Actor
    else {
        type_search = 2;
        document.getElementById('type-film').className = "not-active"
        document.getElementById('type-actor').className = "active";
    }
}

//-----------------------Step 3-----------------------------------------------------------------------
// Detail Movie + Review

var cur_review_page = 1;
// Details movie
async function detailMovie(movie_id) {
    $('.container').empty();
    $('.pagination').empty();

    const response = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}?api_key=c07b3118bd893458a9c721e838c62af0`);
    const data = await response.json();

    const response_p = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/credits?api_key=c07b3118bd893458a9c721e838c62af0`);
    const people = await response_p.json();

    var poster = "https://image.tmdb.org/t/p/w500" + data.poster_path;
    if(data.poster_path == null)
    {
        poster = "images/movie.jpg"
    }

    var company;
    if(data.production_companies[0] == null)
    {
        company = "unknown";
    }
    else
    {
        company = data.production_companies[0].name;
    }

    $('.container').append(`
    <!-- Portfolio Item Heading -->
    <h1 class="my-4" style="color: red">${data.title}
      <small>${data.release_date}</small>
    </h1>
  
    <!-- Portfolio Item Row -->
    <div class="row">
  
      <div class="col-md-5">
        <img class="img-fluid" src="${poster}" height="550" width="400" style="border: 3px solid red" alt="">
      </div>
  
      <div class="col-md-7">
        <h3 class="my-3" style="color: red">Overview</h3>
        <p style="color: white">${data.overview}</p>
        <h3 class="my-3" style="color: red">Movie Details</h3>
        <ul style="color: white">
          <li>Genres: ${data.genres[0].name}</li>
          <li>Runtime: ${data.runtime} minutes</li>
          <li>Tagline: <i>"${data.tagline}"</i></li>
          <li>Rate: ${data.vote_average}</li>
          <li>Director: ${people.crew[0].name}</li>
          <li>Production companies: ${company}</li>
        </ul>
      </div>

      <div class="col-md-7">
        <h3 class="my-3" style="color: red">Actor/Actress</h3>
        <ul style="color: white">
          <li><a onclick="detailPeople('${people.cast[0].id}')">${people.cast[0].name}</a> <p style="color: grey">${people.cast[0].character}</p></li>
          <li><a onclick="detailPeople('${people.cast[1].id}')">${people.cast[1].name}</a> <p style="color: grey">${people.cast[1].character}</p></li>
          <li><a onclick="detailPeople('${people.cast[2].id}')">${people.cast[2].name}</a> <p style="color: grey">${people.cast[2].character}</p></li>
          <li><a onclick="detailPeople('${people.cast[3].id}')">${people.cast[3].name}</a> <p style="color: grey">${people.cast[3].character}</p></li>
        </ul>
      </div>
  
    </div>
    <!-- /.row -->
  
    <!-- Related Projects Row -->
 
    <div class="row"> 
    </div>
    <!-- /.row -->

    <!--  Review -->
    <h2 style="color: red">Review: </h2>
    <div class="review_list"  ></div>
    <div class="pagination_review"></div>
    `)

    const response_1 = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=c07b3118bd893458a9c721e838c62af0`);
    const review = await response_1.json();
    const pages = review.total_pages;
    
    cur_review_page = 1;
    loadPageReivew(movie_id,1);
    addPaginationReview(pages,movie_id)
}

// Load page review
async function loadPageReivew(movie_id,page)
{
    $('.review_list').empty();
    const response_1 = await fetch(`https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=c07b3118bd893458a9c721e838c62af0&page=${page}`);
    const review = await response_1.json();
   
    const list = review.results;

    for(const a of list)
    {
        $('.review_list').append(`
        
            <div class="col-md-12">
                <h4 style="color: red">${a.author}: </h4>
                <p style="color: white">${a.content}</p>
            </div>
        `)
    }

    document.getElementById(`page_review_link${cur_review_page}`).style.backgroundColor = "black";
    document.getElementById(`page_review_link${page}`).style.backgroundColor = "red";
    cur_review_page = page;
}

// Pagination for Review
async function addPaginationReview(pages, movie_id) {

    $('.pagination_review').empty();

    $('.pagination_review').append(`<a href="" style="width: 50px; text-align:center">&laquo;</a>`);

    if (type_search == 1) {
        var i = 1;
        for (i; i <= pages; i++) {
            $('.pagination_review').append(`
            <a href="#" id="page_review_link${i}" style="width: 50px; text-align:center" onclick="loadPageReview(${movie_id},'${i}')">${i}</a>`)
        }
    }
    else {
        var i = 1;
        for (i; i <= pages; i++) {
            $('.pagination_review').append(`
            <a href="#" id="page_review_link${i}" style="width: 50px; text-align:center" onclick="loadPageReview(${movie_id},'${i}')">${i}</a>`)
        }
    }

    $('.pagination_review').append(`<a href="" style="width: 50px; text-align:center" >&raquo;</a>`)
}


//-----------------------Step 4-----------------------------------------------------------------------
// Detail People

// Load detail people
async function detailPeople(people_id)
{
    $('.container').empty();
    $('.pagination').empty();

    const response = await fetch(`https://api.themoviedb.org/3/person/${people_id}?api_key=c07b3118bd893458a9c721e838c62af0`);
    const data = await response.json();

    const response_2 = await fetch(`https://api.themoviedb.org/3/search/person?api_key=c07b3118bd893458a9c721e838c62af0&query=${data.name}`);
    const data_2 = await response_2.json();
    const list_film = data_2.results[0].known_for;


    var profile = "https://image.tmdb.org/t/p/w500" + data.profile_path;
    if(data.profile_path == null)
    {
        profile = "images/movie.jpg"
    }


    $('.container').append(`
    <!-- Portfolio Item Heading -->
    <h1 class="my-4" style="color: red">${data.name}
      <small>${data.birthday}</small>
    </h1>
  
    <!-- Portfolio Item Row -->
    <div class="row">
  
      <div class="col-md-5">
        <img class="img-fluid" src="${profile}" height="550" width="400" style="border: 3px solid red" alt="">
      </div>
  
      <div class="col-md-7">
        <h3 class="my-3" style="color: red">Biography</h3>
        <p style="color: white">${data.biography}</p>
      </div>


  
    </div>
    <!-- /.row -->
  
 
    <h2 style="color: blue">Some movies of ${data.name} participated:</h2>
    <div class="row" style="margin-top: 40px"> 
        <div class="col-md-4 col-sm-6 mb-4" style="margin-right: 20px">
            <h4 style="color: red">${list_film[0].title} <p style="color: grey">${list_film[0].release_date}</p></h4>
            <a href="#" onclick="detailMovie('${list_film[0].id}')">
                <img class="img-fluid" src="https://image.tmdb.org/t/p/w500${list_film[0].poster_path}" width="350" height="400" alt="${list_film[0].title}" style="border: 1px solid red">
            </a>
        </div>
        <div class="col-md-4 col-sm-6 mb-4" style="margin-right: 20px">
        <h4 style="color: red">${list_film[1].title} <p style="color: grey">${list_film[1].release_date}</p></h4>
            <a href="#" onclick="detailMovie('${list_film[1].id}')">
                <img class="img-fluid" src="https://image.tmdb.org/t/p/w500${list_film[1].poster_path}" width="350" height="400" alt="${list_film[1].title}" style="border: 1px solid red">
            </a>
        </div>
        <div class="col-md-3 col-sm-6 mb-4" >
        <h4 style="color: red">${list_film[2].title} <p style="color: grey">${list_film[2].release_date}</p></h4>
            <a href="#" onclick="detailMovie('${list_film[2].id}')">
                <img class="img-fluid" src="https://image.tmdb.org/t/p/w500${list_film[2].poster_path}" width="350" height="400" alt="${list_film[2].title}" style="border: 1px solid red">
            </a>
        </div>
    </div>

    <!-- /.row -->
    `)

}